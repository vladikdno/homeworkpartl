package com.company;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class Main {

    public static void main(String[] args) {

        Map<String, Integer> listOfProducts = new HashMap<>();//создали список продуктов через Map, для того, чтобы через ключ мы могли добавлять кол-во продуктов
        listOfProducts.put("Apple", 10);
        listOfProducts.put("Banana", 25);
        listOfProducts.put("Kiwi", 40);
        listOfProducts.put("Orange", 20);

        Map.Entry<String, Integer>[] input = listOfProducts.entrySet().toArray(new Map.Entry[0]);//копипаст
        Random random = new Random();//рандомное число из от 0-4, потому что массив элементов составляет 4

        new Thread(new Runnable() {//создали первый поток для поставщиков
            @Override
            public void run() {
                for (int i = 0; i < 10; i++) {//запускаем цикл для
                    synchronized (Main.class) {//синхронизируем поток, но не уверен что
                        Map.Entry<String, Integer> key = input[random.nextInt(input.length)];//из массвиа ключей продуктов вытаскиваю рандомный продукт
                        key.setValue(key.getValue() + random.nextInt(input.length));//к массиву продуктов прибавляю рандомное количество продуктов
                        listOfProducts.put(key.getKey(), key.getValue());//к массиву продуктов записываю ключ + значение
                        System.out.println("Provider: " + key);
                        try {
                            Thread.sleep(1000);//delay
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }).start();

        new Thread(new Runnable() {//same
            @Override
            public void run() {
                for (int i = 0; i < 10; i++) {
                    synchronized (Main.class) {
                        Map.Entry<String, Integer> key = input[random.nextInt(input.length)];
                        key.setValue(key.getValue() - random.nextInt(input.length));
                        listOfProducts.put(key.getKey(), key.getValue());
                        System.out.println("Buyer: " + key);
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }).start();
    }
}